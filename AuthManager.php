<?php
/**
 * AuthManager class file.
 *
 * @author Augusto Rheinheimer
 */

/**
 * MyAuthManager extends CDbAuthManager to adapt to our structure
 *
 * The database connection is specified by {@link connectionID}. And the database schema
 * should be as described in "framework/web/auth/*.sql". You may change the names of
 * the three tables used to store the authorization data by setting {@link itemTable},
 * {@link itemChildTable} and {@link assignmentTable}.
 *
 * @property array $authItems The authorization items of the specific type.
 *
 */
class AuthManager extends CAuthManager
{
    /**
     * @var string the ID of the {@link CDbConnection} application component. Defaults to 'db'.
     * The database must have the tables as declared in "framework/web/auth/*.sql".
     */
    public $connectionID='db';
    /**
     * @var string the name of the table storing authorization items. Defaults to 'AuthItem'.
     */
    public $itemTable='AuthItem';
    /**
     * @var string the name of the table storing authorization item hierarchy. Defaults to 'AuthItemChild'.
     */
    public $itemChildTable='AuthItemChild';
    /**
     * @var string the name of the table storing authorization item assignments. Defaults to 'AuthAssignment'.
     */
    public $assignmentTable='AuthAssignment';
    /**
     * @var CDbConnection the database connection. By default, this is initialized
     * automatically as the application component whose ID is indicated as {@link connectionID}.
     */
    public $db;

    private $_usingSqlite;

    /**
     * Initializes the application component.
     * This method overrides the parent implementation by establishing the database connection.
     */
    public function init()
    {
        parent::init();
        $this->_usingSqlite=!strncmp($this->getDbConnection()->getDriverName(),'sqlite',6);
    }

    /**
     * Performs access check for the specified user.
     * @param string $itemName the name of the operation that need access check
     * @param mixed $userId the user ID. This should can be either an integer and a string representing
     * the unique identifier of a user. See {@link IWebUser::getId}.
     * @param array $params name-value pairs that would be passed to biz rules associated
     * with the tasks and roles assigned to the user.
     * Since version 1.1.11 a param with name 'userId' is added to this array, which holds the value of <code>$userId</code>.
     * @return boolean whether the operations can be performed by the user.
     */
    public function checkAccess($itemName,$userId,$params=array())
    {
        $assignments=$this->getAuthAssignments($userId);
        return $this->checkAccessRecursive($itemName,$userId,$params,$assignments);
    }

    /**
     * Performs access check for the specified user.
     * This method is internally called by {@link checkAccess}.
     * @param string $itemName the name of the operation that need access check
     * @param mixed $userId the user ID. This should can be either an integer and a string representing
     * the unique identifier of a user. See {@link IWebUser::getId}.
     * @param array $params name-value pairs that would be passed to biz rules associated
     * with the tasks and roles assigned to the user.
     * Since version 1.1.11 a param with name 'userId' is added to this array, which holds the value of <code>$userId</code>.
     * @param array $assignments the assignments to the specified user
     * @return boolean whether the operations can be performed by the user.
     * @since 1.1.3
     */
    protected function checkAccessRecursive($itemName,$userId,$params,$assignments)
    {
        if(($item=$this->getAuthItem($itemName))===null)
            return false;
        Yii::trace('Checking permission "'.$item->getName().'"','system.web.auth.CDbAuthManager');
        if(!isset($params['userId']))
            $params['userId'] = $userId;
        if($this->executeBizRule($item->getBizRule(),$params,$item->getData()))
        {
            if(in_array($itemName,$this->defaultRoles))
                return true;
            if(isset($assignments[$itemName]))
            {
                $assignment=$assignments[$itemName];
                if($this->executeBizRule($assignment->getBizRule(),$params,$assignment->getData()))
                    return true;
            }
            $parents=$this->db->createCommand()
                ->select('NVL(PAI,\'\') AS PAI')
                ->from($this->itemChildTable)
                ->where('FILHO=:NAME', array(':NAME'=>$itemName))
                ->queryColumn();
            foreach($parents as $parent)
            {
                if($this->checkAccessRecursive($parent,$userId,$params,$assignments))
                    return true;
            }
        }
        return false;
    }

    /**
     * Adds an item as a child of another item.
     * @param string $itemName the parent item name
     * @param string $childName the child item name
     * @return boolean whether the item is added successfully
     * @throws CException if either parent or child doesn't exist or if a loop has been detected.
     */
    public function addItemChild($itemName,$childName)
    {
        if($itemName===$childName)
            throw new CException(Yii::t('yii','Cannot add "{name, Yii::app()->user->id}" as a child of itself.',
                    array('{name}'=>$itemName)));

        $rows=$this->db->createCommand()
            ->select('NM_AUTT_ITEM,TP_AUTT_ITEM,NVL(DSC_AUTT_ITEM,\'\') AS DSC_AUTT_ITEM ,NVL(DSC_REGRA_NGCO,\'\') AS DSC_REGRA_NGCO,NVL(DADO,\'\') AS DADO')
            ->from($this->itemTable)
            ->where('NM_AUTT_ITEM=:NAME1 OR NM_AUTT_ITEM=:NAME2', array(
                ':NAME1'=>$itemName,
                ':NAME2'=>$childName
            ))
            ->queryAll();

        if(count($rows)==2)
        {
            if($rows[0]['NM_AUTT_ITEM']===$itemName)
            {
                $parentType=$rows[0]['TP_AUTT_ITEM'];
                $childType=$rows[1]['TP_AUTT_ITEM'];
            }
            else
            {
                $childType=$rows[0]['TP_AUTT_ITEM'];
                $parentType=$rows[1]['TP_AUTT_ITEM'];
            }
            $this->checkItemChildType($parentType,$childType);
            if($this->detectLoop($itemName,$childName))
                throw new CException(Yii::t('yii','Cannot add "{child}" as a child of "{name}". A loop has been detected.',
                    array('{child}'=>$childName,'{name}'=>$itemName)));

            $this->db->createCommand()
                ->insert($this->itemChildTable, array(
                    'PAI'=>$itemName,
                    'FILHO'=>$childName,
                ));

            return true;
        }
        else
            throw new CException(Yii::t('yii','Either "{parent}" or "{child}" does not exist.',array('{child}'=>$childName,'{parent}'=>$itemName)));
    }

    /**
     * Removes a child from its parent.
     * Note, the child item is not deleted. Only the parent-child relationship is removed.
     * @param string $itemName the parent item name
     * @param string $childName the child item name
     * @return boolean whether the removal is successful
     */
    public function removeItemChild($itemName,$childName)
    {
        return $this->db->createCommand()
            ->delete($this->itemChildTable, 'PAI=:PAI AND FILHO=:FILHO', array(
                ':PAI'=>$itemName,
                ':FILHO'=>$childName
            )) > 0;
    }

    /**
     * Returns a value indicating whether a child exists within a parent.
     * @param string $itemName the parent item name
     * @param string $childName the child item name
     * @return boolean whether the child exists
     */
    public function hasItemChild($itemName,$childName)
    {
        return $this->db->createCommand()
            ->select('NVL(PAI,\'\') AS PAI')
            ->from($this->itemChildTable)
            ->where('PAI=:PAI AND FILHO=:FILHO', array(
                ':PAI'=>$itemName,
                ':FILHO'=>$childName))
            ->queryScalar() !== false;
    }

    /**
     * Returns the children of the specified item.
     * @param mixed $names the parent item name. This can be either a string or an array.
     * The latter represents a list of item names.
     * @return array all child items of the parent
     */
    public function getItemChildren($names)
    {
        if(is_string($names))
            $condition='PAI='.$this->db->quoteValue($names);
        else if(is_array($names) && $names!==array())
        {
            foreach($names as &$name)
                $name=$this->db->quoteValue($name);
            $condition='PAI IN ('.implode(', ',$names).')';
        }

        $rows=$this->db->createCommand()
            ->select('NM_AUTT_ITEM,TP_AUTT_ITEM,NVL(DSC_AUTT_ITEM,\'\') AS DSC_AUTT_ITEM ,NVL(DSC_REGRA_NGCO,\'\') AS DSC_REGRA_NGCO,NVL(DADO,\'\') AS DADO')
            ->from(array(
                $this->itemTable,
                $this->itemChildTable
            ))
            ->where($condition.' AND NM_AUTT_ITEM=FILHO')
            ->queryAll();

        $children=array();
        foreach($rows as $row)
        {
            if(($data=@unserialize($row['DADO']))===false)
                $data=null;
            $children[$row['NM_AUTT_ITEM']]=new CAuthItem($this,$row['NM_AUTT_ITEM'],$row['TP_AUTT_ITEM'],$row['DSC_AUTT_ITEM'],$row['DSC_REGRA_NGCO'],$data);
        }
        return $children;
    }

    /**
     * Assigns an authorization item to a user.
     * @param string $itemName the item name
     * @param mixed $userId the user ID (see {@link IWebUser::getId})
     * @param string $bizRule the business rule to be executed when {@link checkAccess} is called
     * for this particular authorization item.
     * @param mixed $data additional data associated with this assignment
     * @return CAuthAssignment the authorization assignment information.
     * @throws CException if the item does not exist or if the item has already been assigned to the user
     */
    public function assign($itemName,$userId,$bizRule=null,$data=null)
    {
        if($this->usingSqlite() && $this->getAuthItem($itemName)===null)
            throw new CException(Yii::t('yii','The item "{name}" does not exist.',array('{name}'=>$itemName)));

        $this->db->createCommand()
            ->insert($this->assignmentTable, array(
                'NM_AUTT_ASSOC'=>$itemName,
                'ID_USU'=>$userId,
                'DSC_REGRA_NGCO'=>$bizRule,
                'DADO'=>serialize($data)
            ));
        return new CAuthAssignment($this,$itemName,$userId,$bizRule,$data);
    }

    /**
     * Revokes an authorization assignment from a user.
     * @param string $itemName the item name
     * @param mixed $userId the user ID (see {@link IWebUser::getId})
     * @return boolean whether removal is successful
     */
    public function revoke($itemName,$userId)
    {
        return $this->db->createCommand()
            ->delete($this->assignmentTable, 'NM_AUTT_ASSOC=:ITEMNAME AND ID_USU=:IDUSU', array(
                ':ITEMNAME'=>$itemName,
                ':IDUSU'=>$userId
            )) > 0;
    }

    /**
     * Returns a value indicating whether the item has been assigned to the user.
     * @param string $itemName the item name
     * @param mixed $userId the user ID (see {@link IWebUser::getId})
     * @return boolean whether the item has been assigned to the user.
     */
    public function isAssigned($itemName,$userId)
    {
        return $this->db->createCommand()
            ->select('NM_AUTT_ASSOC')
            ->from($this->assignmentTable)
            ->where('NM_AUTT_ASSOC=:ITEMNAME AND ID_USU=:IDUSU', array(
                ':ITEMNAME'=>$itemName,
                ':IDUSU'=>$userId))
            ->queryScalar() !== false;
    }

    /**
     * Returns the item assignment information.
     * @param string $itemName the item name
     * @param mixed $userId the user ID (see {@link IWebUser::getId})
     * @return CAuthAssignment the item assignment information. Null is returned if
     * the item is not assigned to the user.
     */
    public function getAuthAssignment($itemName,$userId)
    {
        $row=$this->db->createCommand()
            ->select('NM_AUTT_ASSOC,ID_USU,NVL(DSC_REGRA_NGCO,\'\') AS DSC_REGRA_NGCO,NVL(DADO,\'\') AS DADO')
            ->from($this->assignmentTable)
            ->where('NM_AUTT_ASSOC=:ITEMNAME AND ID_USU=:IDUSU', array(
                ':ITEMNAME'=>$itemName,
                ':IDUSU'=>$userId))
            ->queryRow();
        if($row!==false)
        {
            if(($data=@unserialize($row['DADO']))===false)
                $data=null;
            return new CAuthAssignment($this,$row['NM_AUTT_ASSOC'],$row['ID_USU'],$row['DSC_REGRA_NGCO'],$data);
        }
        else
            return null;
    }

    /**
     * Returns the item assignments for the specified user.
     * @param mixed $userId the user ID (see {@link IWebUser::getId})
     * @return array the item assignment information for the user. An empty array will be
     * returned if there is no item assigned to the user.
     */
    public function getAuthAssignments($userId)
    {
        $rows=$this->db->createCommand()
            ->select('NM_AUTT_ASSOC,ID_USU,NVL(DSC_REGRA_NGCO,\'\') AS DSC_REGRA_NGCO,NVL(DADO,\'\') AS DADO')
            ->from($this->assignmentTable)
            ->where('ID_USU=:IDUSU', array(':IDUSU'=>$userId))
            ->queryAll();
        $assignments=array();
        foreach($rows as $row)
        {
            if(($data=@unserialize($row['DADO']))===false)
                $data=null;
            $assignments[$row['NM_AUTT_ASSOC']]=new CAuthAssignment($this,$row['NM_AUTT_ASSOC'],$row['ID_USU'],$row['DSC_REGRA_NGCO'],$data);
        }
        return $assignments;
    }

    /**
     * Saves the changes to an authorization assignment.
     * @param CAuthAssignment $assignment the assignment that has been changed.
     */
    public function saveAuthAssignment($assignment)
    {
        $this->db->createCommand()
            ->update($this->assignmentTable, array(
                'DSC_REGRA_NGCO'=>$assignment->getBizRule(),
                'DADO'=>serialize($assignment->getData()),
            ), 'NM_AUTT_ASSOC=:ITEMNAME AND ID_USU=:IDUSU', array(
                ':ITEMNAME'=>$assignment->getItemName(),
                ':IDUSU'=>$assignment->getUserId()
            ));
    }

    /**
     * Returns the authorization items of the specific type and user.
     * @param integer $type the item type (0: operation, 1: task, 2: role). Defaults to null,
     * meaning returning all items regardless of their type.
     * @param mixed $userId the user ID. Defaults to null, meaning returning all items even if
     * they are not assigned to a user.
     * @return array the authorization items of the specific type.
     */
    public function getAuthItems($type=null,$userId=null)
    {
        if($type===null && $userId===null)
        {
            $command=$this->db->createCommand()
                ->select('NM_AUTT_ITEM,TP_AUTT_ITEM,NVL(DSC_AUTT_ITEM,\'\') AS DSC_AUTT_ITEM ,NVL(DSC_REGRA_NGCO,\'\') AS DSC_REGRA_NGCO,NVL(DADO,\'\') AS DADO')
                ->from($this->itemTable);
        }
        else if($userId===null)
        {
            $command=$this->db->createCommand()
                ->select('NM_AUTT_ITEM,TP_AUTT_ITEM,NVL(DSC_AUTT_ITEM,\'\') AS DSC_AUTT_ITEM ,NVL(DSC_REGRA_NGCO,\'\') AS DSC_REGRA_NGCO,NVL(DADO,\'\') AS DADO')
                ->from($this->itemTable)
                ->where('TP_AUTT_ITEM=:TYPE', array(':TYPE'=>$type));
        }
        else if($type===null)
        {
            $command=$this->db->createCommand()
                ->select('NM_AUTT_ITEM,TP_AUTT_ITEM,NVL(DSC_AUTT_ITEM,\'\') AS DSC_AUTT_ITEM ,NVL("T1"."DSC_REGRA_NGCO",\'\') AS DSC_REGRA_NGCO,NVL("T1"."DADO",\'\') AS DADO')
                ->from(array(
                    $this->itemTable.' T1',
                    $this->assignmentTable.' T2'
                ))
                ->where('NM_AUTT_ITEM=NM_AUTT_ASSOC AND ID_USU=:userid', array(':userid'=>$userId));
        }
        else
        {
            $command=$this->db->createCommand()
                ->select('NM_AUTT_ITEM,TP_AUTT_ITEM,NVL(DSC_AUTT_ITEM,\'\') AS DSC_AUTT_ITEM ,NVL("T1"."DSC_REGRA_NGCO",\'\') AS DSC_REGRA_NGCO,NVL("T1"."DADO",\'\') AS DADO')
                ->from(array(
                    $this->itemTable.' T1',
                    $this->assignmentTable.' T2'
                ))
                ->where('NM_AUTT_ITEM=NM_AUTT_ASSOC  AND TP_AUTT_ITEM=:TYPE AND ID_USU=:IDUSU', array(
                    ':TYPE'=>$type,
                    ':IDUSU'=>$userId
                ));
        }
        $items=array();
        foreach($command->queryAll() as $row)
        {
            if(($data=@unserialize($row['DADO']))===false)
                $data=null;
            $items[$row['NM_AUTT_ITEM']]=new CAuthItem($this,$row['NM_AUTT_ITEM'],$row['TP_AUTT_ITEM'],$row['DSC_AUTT_ITEM'],$row['DSC_REGRA_NGCO'],$data);
        }
        return $items;
    }

    /**
     * Creates an authorization item.
     * An authorization item represents an action permission (e.g. creating a post).
     * It has three types: operation, task and role.
     * Authorization items form a hierarchy. Higher level items inheirt permissions representing
     * by lower level items.
     * @param string $name the item name. This must be a unique identifier.
     * @param integer $type the item type (0: operation, 1: task, 2: role).
     * @param string $description description of the item
     * @param string $bizRule business rule associated with the item. This is a piece of
     * PHP code that will be executed when {@link checkAccess} is called for the item.
     * @param mixed $data additional data associated with the item.
     * @return CAuthItem the authorization item
     * @throws CException if an item with the same name already exists
     */
    public function createAuthItem($name,$type,$description='',$bizRule=null,$data=null)
    {
        $this->db->createCommand()
            ->insert($this->itemTable, array(
                'NM_AUTT_ITEM'=>$name,
                'TP_AUTT_ITEM'=>$type,
                'DSC_AUTT_ITEM'=>$description,
                'DSC_REGRA_NGCO'=>$bizRule,
                'DADO'=>serialize($data)
            ));
        return new CAuthItem($this,$name,$type,$description,$bizRule,$data);
    }

    /**
     * Removes the specified authorization item.
     * @param string $name the name of the item to be removed
     * @return boolean whether the item exists in the storage and has been removed
     */
    public function removeAuthItem($name)
    {
        if($this->usingSqlite())
        {
            $this->db->createCommand()
                ->delete($this->itemChildTable, 'PAI=:NAME1 OR FILHO=:NAME2', array(
                    ':NAME1'=>$name,
                    ':NAME2'=>$name
            ));
            $this->db->createCommand()
                ->delete($this->assignmentTable, 'NM_AUTT_ASSOC=:NAME', array(
                    ':NAME'=>$name,
            ));
        }

        return $this->db->createCommand()
            ->delete($this->itemTable, 'NM_AUTT_ITEM=:NAME', array(
                ':NAME'=>$name
            )) > 0;
    }

    /**
     * Returns the authorization item with the specified name.
     * @param string $name the name of the item
     * @return CAuthItem the authorization item. Null if the item cannot be found.
     */
    public function getAuthItem($name)
    {
        $row=$this->db->createCommand()
            ->select('NM_AUTT_ITEM,TP_AUTT_ITEM,NVL(DSC_AUTT_ITEM,\'\') AS DSC_AUTT_ITEM ,NVL(DSC_REGRA_NGCO,\'\') AS DSC_REGRA_NGCO,NVL(DADO,\'\') AS DADO')
            ->from($this->itemTable)
            ->where('NM_AUTT_ITEM=:NAME', array(':NAME'=>$name))
            ->queryRow();

        if($row!==false)
        {
            if(($data=@unserialize($row['DADO']))===false)
                $data=null;
            return new CAuthItem($this,$row['NM_AUTT_ITEM'],$row['TP_AUTT_ITEM'],$row['DSC_AUTT_ITEM'],$row['DSC_REGRA_NGCO'],$data);
        }
        else
            return null;
    }

    /**
     * Saves an authorization item to persistent storage.
     * @param CAuthItem $item the item to be saved.
     * @param string $oldName the old item name. If null, it means the item name is not changed.
     */
    public function saveAuthItem($item,$oldName=null)
    {
        if($this->usingSqlite() && $oldName!==null && $item->getName()!==$oldName)
        {
            $this->db->createCommand()
                ->update($this->itemChildTable, array(
                    'PAI'=>$item->getName(),
                ), 'PAI=:NAME', array(
                    ':NAME'=>$oldName,
                ));
            $this->db->createCommand()
                ->update($this->itemChildTable, array(
                    'FILHO'=>$item->getName(),
                ), 'FILHO=:NAME', array(
                    ':NAME'=>$oldName,
                ));
            $this->db->createCommand()
                ->update($this->assignmentTable, array(
                    'NM_AUTT_ASSOC'=>$item->getName(),
                ), 'NM_AUTT_ASSOC=:NAME', array(
                    ':NAME'=>$oldName,
                ));
        }

        $this->db->createCommand()
            ->update($this->itemTable, array(
                'NM_AUTT_ITEM'=>$item->getName(),
                'TP_AUTT_ITEM'=>$item->getType(),
                'DSC_AUTT_ITEM'=>$item->getDescription(),
                'DSC_REGRA_NGCO'=>$item->getBizRule(),
                'DADO'=>serialize($item->getData()),
            ), 'NM_AUTT_ITEM=:NAME', array(
                ':NAME'=>$oldName===null?$item->getName():$oldName,
            ));
    }

    /**
     * Saves the authorization data to persistent storage.
     */
    public function save()
    {
    }

    /**
     * Removes all authorization data.
     */
    public function clearAll()
    {
        $this->clearAuthAssignments();
        $this->db->createCommand()->delete($this->itemChildTable);
        $this->db->createCommand()->delete($this->itemTable);
    }

    /**
     * Removes all authorization assignments.
     */
    public function clearAuthAssignments()
    {
        $this->db->createCommand()->delete($this->assignmentTable);
    }

    /**
     * Checks whether there is a loop in the authorization item hierarchy.
     * @param string $itemName parent item name
     * @param string $childName the name of the child item that is to be added to the hierarchy
     * @return boolean whether a loop exists
     */
    protected function detectLoop($itemName,$childName)
    {
        if($childName===$itemName)
            return true;
        foreach($this->getItemChildren($childName) as $child)
        {
            if($this->detectLoop($itemName,$child->getName()))
                return true;
        }
        return false;
    }

    /**
     * @return CDbConnection the DB connection instance
     * @throws CException if {@link connectionID} does not point to a valid application component.
     */
    protected function getDbConnection()
    {
        if($this->db!==null)
            return $this->db;
        else if(($this->db=Yii::app()->getComponent($this->connectionID)) instanceof CDbConnection)
            return $this->db;
        else
            throw new CException(Yii::t('yii','CDbAuthManager.connectionID "{id}" is invalid. Please make sure it refers to the ID of a CDbConnection application component.',
                array('{id}'=>$this->connectionID)));
    }

    /**
     * @return boolean whether the database is a SQLite database
     */
    protected function usingSqlite()
    {
        return $this->_usingSqlite;
    }
}