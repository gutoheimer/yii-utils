<?php

/**
 * compareDateValidator class file
 *
 * Validator to compare two dates, works similarly to CCompareValidator.
 *
 * @author Augusto Rheinheimer
 */
class compareDate extends CCompareValidator
{   
    /**
     * @var string the date format used to compare the values.
     */
    public $dateFormat = 'Y-m-d H:i:s';
    
    
    /**
     * @var string the date format used to compare the values ISO-8601 formats are supported across all browsers Uses moment.js.
     */
    public $jsDateFormat = '"YYYY-MM-DDTHH:mm:ss"';

    protected function validateAttribute($object, $attribute)
    {
        $value = $object->$attribute;

        if ($this->allowEmpty && $this->isEmpty($value)) {
            return;
        }

        if ($this->compareValue !== null) {
            $compareTo = $compareValue = $this->compareValue;
        }
        else {
            $compareAttribute = $this->compareAttribute === null ? $attribute . '_repeat' : $this->compareAttribute;
            $compareValue = $object->$compareAttribute;
            $compareTo = $object->getAttributeLabel($compareAttribute);
        }
        $compareDate = DateTime::createFromFormat($this->dateFormat, $compareValue);
        $date = DateTime::createFromFormat($this->dateFormat, $value);

        // make sure we have two dates
        if ($date instanceof DateTime && $compareDate instanceof DateTime)
            $diff = ((integer) $date->diff($compareDate)->format('%r%a%H%I%S')) * -1;
        else
            return; // Perhaps not the best way of handling this. Possibly add an error message.

        switch ($this->operator) {
            case '=':
            case '==':
                if ($diff != 0) {
                    $message = $this->message !== null ? $this->message : Yii::t('yii', '{attribute} must be repeated exactly.');
                    $this->addError($object, $attribute, $message, array('{compareAttribute}' => $compareTo));
                }
                break;
            case '!=':
                if ($diff == 0) {
                    $message = $this->message !== null ? $this->message : Yii::t('yii', '{attribute} must not be equal to "{compareValue}".');
                    $this->addError($object, $attribute, $message, array('{compareAttribute}' => $compareTo, '{compareValue}' => $compareValue));
                }
                break;
            case '>':
                if ($diff <= 0) {
                    $message = $this->message !== null ? $this->message : Yii::t('yii', '{attribute} must be greater than "{compareValue}".');
                    $this->addError($object, $attribute, $message, array('{compareAttribute}' => $compareTo, '{compareValue}' => $compareValue));
                }
                break;
            case '>=':
                if ($diff < 0) {
                    $message = $this->message !== null ? $this->message : Yii::t('yii', '{attribute} must be greater than or equal to "{compareValue}".');
                    $this->addError($object, $attribute, $message, array('{compareAttribute}' => $compareTo, '{compareValue}' => $compareValue));
                }
                break;
            case '<':
                if ($diff >= 0) {
                    $message = $this->message !== null ? $this->message : Yii::t('yii', '{attribute} must be less than "{compareValue}".');
                    $this->addError($object, $attribute, $message, array('{compareAttribute}' => $compareTo, '{compareValue}' => $compareValue));
                }
                break;
            case '<=':
                if ($diff > 0) {
                    $message = $this->message !== null ? $this->message : Yii::t('yii', '{attribute} must be less than or equal to "{compareValue}".');
                    $this->addError($object, $attribute, $message, array('{compareAttribute}' => $compareTo, '{compareValue}' => $compareValue));
                }
                break;
            default:
                throw new CException(Yii::t('yii', 'Invalid operator "{operator}".', array('{operator}' => $this->operator)));
        }
    }

    public function clientValidateAttribute($object, $attribute)
    {
        Yii::app()->clientScript->registerScriptFile(Yii::app()->getBaseUrl() . '/js/moment.min.js');
        
        if ($this->compareValue !== null) {
            $compareTo = $this->compareValue;
            $compareValue = "moment(".CJSON::encode($this->compareValue).",'".$this->jsDateFormat."').toDate().getTime()";
        }
        else {
            $compareAttribute = $this->compareAttribute === null ? $attribute . '_repeat' : $this->compareAttribute;
            $compareValue = "moment(\$('#" . (CHtml::activeId($object, $compareAttribute)) . "').val().replace(' ', 'T'),'".$this->jsDateFormat."').toDate().getTime()";
            $compareTo = $object->getAttributeLabel($compareAttribute);
        }

        $message = $this->message;
        $jsDate = "moment(value.replace(' ', 'T'),'".$this->jsDateFormat."').toDate().getTime()";

        switch ($this->operator) {
            case '=':
            case '==':
                if ($message === null)
                    $message = Yii::t('yii', '{attribute} must be repeated exactly.');
                $condition = "{$jsDate} != {$compareValue}";
                break;
            case '!=':
                if ($message === null)
                    $message = Yii::t('yii', '{attribute} must not be equal to "{compareValue}".');
                $condition = "{$jsDate} == {$compareValue}";
                break;
            case '>':
                if ($message === null)
                    $message = Yii::t('yii', '{attribute} must be greater than "{compareValue}".');
                $condition = "{$jsDate} <= {$compareValue}";
                break;
            case '>=':
                if ($message === null)
                    $message = Yii::t('yii', '{attribute} must be greater than or equal to "{compareValue}".');
                $condition = "{$jsDate} < {$compareValue}";
                break;
            case '<':
                if ($message === null)
                    $message = Yii::t('yii', '{attribute} must be less than "{compareValue}".');
                $condition = "{$jsDate} >= {$compareValue}";
                break;
            case '<=':
                if ($message === null)
                    $message = Yii::t('yii', '{attribute} must be less than or equal to "{compareValue}".');
                $condition = "{$jsDate} > {$compareValue}";
                break;
            default:
                throw new CException(Yii::t('yii', 'Invalid operator "{operator}".', array('{operator}' => $this->operator)));
        }

        $message = strtr($message, array(
            '{attribute}' => $object->getAttributeLabel($attribute),
            '{compareValue}' => $compareTo,
                ));

        return "
if(" . ($this->allowEmpty ? "$.trim(value)!='' && " : '') . $condition . ") {
    messages.push(" . CJSON::encode($message) . ");
}
";
    }

}
?>
