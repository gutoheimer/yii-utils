<?php
/**
 * CNPJValidator class file.
 *
 * @author Augusto Rheinheimer <augusto@api.adv.br>
 */
class cnpj extends CValidator
{
    public $allowEmpty=true;
    
    /**
     * Validates the attribute of the object.
     * If there is any error, the error message is added to the object.
     * @param CModel the data object being validated
     * @param string the name of the attribute to be validated.
     */
    protected function validateAttribute( $object, $attribute )
    {
        $message= $this->message!==null ? $this->message : Yii::t('yii','{attribute} não é um CNPJ válido.');
        if ( !$this->validateValue( $object->$attribute ) )
            $this->addError($object, $attribute, $message);
    }
    
    public function clientValidateAttribute($object,$attribute) 
    {
        $message= $this->message!==null ? $this->message :  Yii::t('yii','{attribute} não é um CNPJ válido.');
        $message=strtr($message, array(
                '{attribute}'=>$object->getAttributeLabel($attribute),
            ));
        $message = CJSON::encode($message);
        
        $js = <<<JS
cnpj = jQuery.trim(value);
    

   cnpj = cnpj.replace('/','');
   cnpj = cnpj.replace('.','');
   cnpj = cnpj.replace('.','');
   cnpj = cnpj.replace('-','');

   var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
   digitos_iguais = 1;

   if (cnpj.length < 14 && cnpj.length < 15){
      messages.push({$message});
   }
   for (i = 0; i < cnpj.length - 1; i++){
      if (cnpj.charAt(i) != cnpj.charAt(i + 1)){
         digitos_iguais = 0;
         break;
      }
   }

   if (!digitos_iguais){
      tamanho = cnpj.length - 2
      numeros = cnpj.substring(0,tamanho);
      digitos = cnpj.substring(tamanho);
      soma = 0;
      pos = tamanho - 7;

      for (i = tamanho; i >= 1; i--){
         soma += numeros.charAt(tamanho - i) * pos--;
         if (pos < 2){
            pos = 9;
         }
      }
      resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
      if (resultado != digitos.charAt(0)){
         messages.push({$message});
      }
      tamanho = tamanho + 1;
      numeros = cnpj.substring(0,tamanho);
      soma = 0;
      pos = tamanho - 7;
      for (i = tamanho; i >= 1; i--){
         soma += numeros.charAt(tamanho - i) * pos--;
         if (pos < 2){
            pos = 9;
         }
      }
      resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
      if (resultado != digitos.charAt(1)){
         messages.push({$message});
      }
      return true;
   }else{
      messages.push($message);
   }
JS;

        if($this->allowEmpty)
        {
            $js=<<<JS
            if(jQuery.trim(value)!='') {
                $js
            }
JS;
        }
        
        return $js;
    }
    
    public function validateValue($value) 
    {
        $value = preg_replace(array('/\./','/\-/','/\//'), '', trim($value));
        $value = str_pad(preg_replace('/[^0-9_]/', '', $value), 14, '0', STR_PAD_LEFT);
        for ($x=0; $x<10; $x++)
            if ( $value == str_repeat($x, 14) )
                return false;


        if ( strlen($value) != 14 ) {
            return false;
        } else {
            for ($t = 12; $t < 14; $t++) {
                $d = 0;
                $c = 0;
                for ($m = $t - 7; $m >= 2; $m--, $c++) {
                    $d += $value{$c} * $m;
                }
                for ($m = 9; $m >= 2; $m--, $c++) {
                    $d += $value{$c} * $m;
                }

                $d = ((10 * $d) % 11) % 10;

                if ($value{$c} != $d) {
                    return false;
                }
            }
            return true;
        }
    }
    
}