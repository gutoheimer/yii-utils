<?php
/**
 * CPFValidator class file.
 *
 * @author Augusto Rheinheimer <augusto@api.adv.br>
 */
class cpf extends CValidator
{
    public $allowEmpty=true;
    
    /**
     * Validates the attribute of the object.
     * If there is any error, the error message is added to the object.
     * @param CModel the data object being validated
     * @param string the name of the attribute to be validated.
     */
    protected function validateAttribute( $object, $attribute )
    {
        $message= $this->message!==null ? $this->message :  Yii::t('yii','{attribute} não é um CPF válido.');
        if ( !$this->validateValue( $object->$attribute ) )
            $this->addError($object, $attribute, $message);
    }
    
    public function clientValidateAttribute($object,$attribute)
    {
        $message= $this->message!==null ? $this->message :  Yii::t('yii','{attribute} não é um CPF válido.');
        $message=strtr($message, array(
                '{attribute}'=>$object->getAttributeLabel($attribute),
            ));
        $message = CJSON::encode($message);
        $js = <<<JS
         value = jQuery.trim(value);
        
        value = value.replace('.','');
        value = value.replace('.','');
        cpf = value.replace('-','');
        while(cpf.length < 11) cpf = "0"+ cpf;
        var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
        var a = [];
        var b = new Number;
        var c = 11;
        for (i=0; i<11; i++){
            a[i] = cpf.charAt(i);
            if (i < 9) b += (a[i] * --c);
        }
        if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
        b = 0;
        c = 11;
        for (y=0; y<10; y++) b += (a[y] * c--);
        if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }
        if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)){
            messages.push({$message});
        };
JS;
        if($this->allowEmpty)
        {
            $js=<<<JS
            if(jQuery.trim(value)!='') {
                $js
            }
JS;
        }
        
        return $js;
    }

    private function validateValue($value)
    {
        $value = preg_replace(array('/\./','/\-/','/\//'), '', trim($value));
        $value = str_pad(preg_replace('/[^0-9_]/', '', $value), 11, '0', STR_PAD_LEFT);

        
        for ($x=0; $x<10; $x++)
            if ( $value == str_repeat($x, 11) )
                return false;

        
        if ( strlen($value) != 11 )
        {
            return false;
        }
        else
        {   
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $value{$c} * (($t + 1) - $c);
                }

                $d = ((10 * $d) % 11) % 10;

                if ($value{$c} != $d) {
                    return false;
                }
            }

            return true;
        }
    }
    
}